

Template.registerHelper('trimInput', function(val){
  return val.replace(/^\s*|\s*$/g, "");
});

Template.registerHelper('tempFile', function(){
  return Tempfiles.findOne(Session.get('tempFileId'));
});

Template.registerHelper('getGeoLocation',function(err,result){
  var geoLocation = Geolocation.currentLocation();
  if (geoLocation) {
    var latlong = {
      lat: geoLocation.coords.latitude,
      long: geoLocation.coords.longitude
    };
    var geoData = [];
    Meteor.call("getAddress", latlong, function(err, formatedAddress) {
      if (err) {
        alert(err.reason);
      } else {
        geoData = {
          userId: Meteor.userId(),
          location: latlong,
          address: formatedAddress,
        };
        Meteor.call('addGeoLocation2Account', geoData, function(err, result) {
          if (err) {
            alert(err.reason);
          } else {
            console.log("*** GeoLocation updated ***");
            //getGeoLocation nur einmal pro Session
            // Session.set('getGeoLocation', true);
          }
        });
      }
    });
  }
});

Template.registerHelper('owner', function(){
  if(Session.get('activeBox')=="menuBox"){
    return Meteor.users.findOne({_id: Meteor.userId()});
  }
  if(Session.get('activeBox')=="profileBox"){
    return Meteor.users.findOne({_id: Session.get('profileUserId')});
  }
    return Meteor.users.findOne({_id: this.ownerId});
});

Template.registerHelper('userpicture', function(){
  var userpicture = this.profile.details.picture;
  if(userpicture) {
    userpicture = Images.findOne(userpicture);
    if(userpicture){
      return userpicture.url();
    } else {
      return "img/Unknown_User.jpg";
    }
  } else {
    return "img/Unknown_User.jpg";
  }
});

Template.registerHelper("notificationsCount", function() {
  var count = Notifications.find(
    {$and: [
      {receiverId: Meteor.userId()},
      {status: "unread"}
      ]}
    ).count();
  if(count > 0) {
    return count;
  }
});

Template.registerHelper("ratingCount", function(){
  //DEV: RatingCount
  return "219";
});

Template.registerHelper("ratingCountLabel", function(){
  //DEV: RatingCountLabel 1 Rating / 2 Ratings
  return "Ratings";
});
