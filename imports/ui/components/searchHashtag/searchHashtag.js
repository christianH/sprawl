Template.searchHashtag.helpers({
  hashtags: function() {
    var searchQuery = Meteor.subscribe('hashtags', Session.get('searchQuery'));
    return Hashtags.find({}, {sort: [['score', 'desc']] });
  },
});

Template.searchHashtag.onRendered(function() {
   Session.setDefault('searchQuery', null);
});

Template.searchHashtag.events({
  'input #searchHashtag-hashtag': function(e) {
    e.preventDefault();
    Session.set('searchQuery', $('#searchHashtag-hashtag').val());
    if(Session.get('searchQuery') === "") {
      $('.search-box').hide();
    } else {
      $('.search-box').show();
    }
  },
  'click .jsSearchBoxToggle': function(e) {
    e.preventDefault();
    Session.set('searchQuery', $('#searchHashtag-hashtag').val());
    $('.search-box').toggle();
  },
  'click .jsSelectHashtag': function(e) {
    e.preventDefault();
    $('#searchHashtag-hashtag').val(e.target.hash);
    Session.set('searchQuery', $('#searchHashtag-hashtag').val());
    $('.search-box').hide();
  },
});
