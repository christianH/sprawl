
Template.loadingSpinner.onRendered(function() {
  if (!Session.get('loadingSplash')) {
    this.loading = window.pleaseWait({
      logo: '/img/logo.svg',
      backgroundColor: '#4071A4',
      loadingHtml: spinner
    });
    // Session.set('loadingSplash', true); // just show loading splash once
  }
});

Template.loadingSpinner.onDestroyed(function(){
  if ( this.loading ) {
    this.loading.finish();
  }
});

var message = '<p class="loading-message">Loading Message</p>';
// var spinner = '<div class="sk-spinner sk-spinner-pulse"></div>';
var spinner = '<div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
