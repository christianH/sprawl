
Template.passwordRecoveryBox.helpers({
  resetPasswordToken : function(t) {
    return Session.get('resetPassword');
  }
});

Template.passwordRecoveryBox.events({

  'submit #passwordRecoveryBox' : function(e, t) {
    e.preventDefault();
    var email = UI._globalHelpers.trimInput(t.find('#recovery-email').value);
    console.log(email);
    Accounts.forgotPassword({email: email}, function(err) {
     if (err) {
       console.log(err.message);
       if (err.message === 'User not found [403]') {
         console.log('This email does not exist.');
       } else {
         console.log('We are sorry but something went wrong.');
       }
     } else {
       console.log('Email Sent. Check your mailbox.');
     }
   });
   //DEV: Password Recovery Welche Box?
   Session.set('activeBox', "loginBox");
   return false;
  },
  'submit #resetPasswordBox' : function(e, t) {
    e.preventDefault();
    var password = t.find('#resetPasswordBox-new-password').value;
    var password2 = t.find('#resetPasswordBox-new-password-2').value;

    if(password === password2) {
      Accounts.resetPassword(Session.get('resetPassword'), password, function(err){
        if (err) {
          console.log("Password Reset Error: " + err.reason);
        }
        else {
          console.log("*** Changed Password successfully ***");
          Session.set('resetPassword', null);
          Session.set('profileUserId', Meteor.userId());
          Session.set('activeBox', "menuBox");
          console.log("*** Logged in successfully ***");
        }
      });
    } else {
      console.log("New passwords don't match!");
      return false;
    }
  },

});
