
Template.loginBox.onRendered(function() {
  if (Accounts._resetPasswordToken) {
    Session.set('resetPassword', Accounts._resetPasswordToken);
  }
});

Template.loginBox.helpers({

});

Template.loginBox.events({
  'submit #loginBox': function(e){
    e.preventDefault();
    var username = $('.loginForm-username').val();
    var password = $('.loginForm-password').val();
    Meteor.loginWithPassword(username, password, function(error){
      if(error){
        console.log(error.reason);
      } else {
        Session.set('profileUserId', Meteor.userId());
        FlowRouter.go('/menu');
      }
    });
    },

});
