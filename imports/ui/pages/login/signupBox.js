function submitSignupBox(form){
  var firstname = form.signupBox_firstname.value;
  var lastname = form.signupBox_lastname.value;
  var homeaddress = form.signupBox_homeaddress.value;
  var email = form.signupBox_email.value;
  var username = form.signupBox_username.value;
  var password = form.signupBox_password.value;
  var password2 = form.signupBox_password2.value;

  var userData = {
    firstname: firstname,
    lastname: lastname,
    homeaddress: homeaddress,
    email: email,
    username: username,
    password: password,
  };

  if(password === password2) {
      if(password.length >= 6) {
        Meteor.call('createAccount', userData, function(error, result) {
            if(error) {
                console.log(error.reason);
            } else {
              console.log("Created Account");
              Meteor.loginWithPassword(username, password, function(error){
                if(error){
                    console.log(error.reason);
                } else {
                  console.log("Automatically logged in");
                  Session.set('activeBox', "menuBox");
                  Session.set('profileUserId', Meteor.userId());
                }
              });
            }
        });
      } else {
        console.log("Password too short. Use at least 6 characters.");
      }
  } else {
   console.log("Password does not match!");
  }
}

Template.signupBox.onRendered(function() {
  this.autorun(function () {
    if (GoogleMaps.loaded()) {
      $('.autocomplete').geocomplete();
    }
  });
});

Template.signupBox.events({

  'submit #signupBox': function(e){
    e.preventDefault();
    submitSignupBox(e.target);
  },

});
