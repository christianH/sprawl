function submitAddSubscriptionBox(form) {
  var ownerId = Meteor.user()._id;
  var ownerName = Meteor.user().username;
  var hashtag = form.searchHashtag_hashtag.value;
  //DEV: validate Hashtag
  //DEV: Limit incoming requests
  //DEV: Set time Period
  var address = Meteor.user().profile.details.homeaddress;
  var location = Meteor.user().profile.details.homelocation;
  var radius = form.addSubscriptionBox_radius.value;
      radius = parseInt(radius, 10);

  var conversationData = {
    ownerId: ownerId,
    ownerName: ownerName,
    hashtag: hashtag,
    loc: {
      type: "Point",
      coordinates: [location.long, location.lat]
    },
    address: address,
    radius: radius,
    timestamp: Date.now() / 1000,
  };

  Meteor.call('createConversation', conversationData, function(err, result) {
    if (err) {
      alert(err.reason);
    } else {
      Meteor.call('addMessage2Conversation', conversationData, function(err, result) {
        if (err) {
          alert(err.reason);
        } else {
          console.log("*** Msg added to Conversation successfully ***");
        }
      });
      form.reset();
      Session.set('activeBox', "inBox");
    }
  });
}


Template.addReplyBox.helpers({
  request: function() {
    return Requests.findOne(Session.get('requestId'));
  },
  tempAttachment: function() {
    return Tempfiles.findOne(Session.get('tempFileId'));
  },
});

Template.addReplyBox.events({
  'click .jsAddAttachment': function(e) {
    e.preventDefault();
    $("#uploadAttachment").trigger('click');
  },
  'change .myFileInput': function(event, template) {
      FS.Utility.eachFile(event, function(file) {
        Tempfiles.remove(Session.get('tempFileId'));
        Tempfiles.insert(file, function (err, fileObj) {
          if (err){
             console.log(err);
          } else {
            Tempfiles.update(
              {_id:fileObj._id},
              {$set: {ownerId: Meteor.userId()}}
            );
            Session.set('tempFileId',fileObj._id);
            console.log("Upload Temp successfull");
          }
        });
     });
   },
   'click .jsDeleteAttachment': function(e) {
     e.preventDefault();
     Tempfiles.remove(Session.get('tempFileId'));
   },
   'submit #addReplyBox': function(e) {
     e.preventDefault();
    //  submitAddReplyBox(e.target);

    var customer = this.ownerId;
    var userId = Meteor.userId();

    var conversationData = {
      ownerIds: [this.ownerId, Meteor.userId()],
      customerId: this.ownerId,
      providerId: Meteor.userId(),
      requestId: this._id,
      hashtag: this.hashtag,
      timestamp: Date.now() / 1000,
      msg: [],
    };

    console.log(conversationData);
    // Meteor.call('createConversation', conversationData, function(err, result) {
    //   if (err) {
    //     alert(err.reason);
    //   } else {
    //     Meteor.call('addMessage2Conversation', conversationData, function(err, result) {
    //       if (err) {
    //         alert(err.reason);
    //       } else {
    //         console.log("*** Msg added to Conversation successfully ***");
    //       }
    //     });
    //     form.reset();
    //     Session.set('activeBox', "inBox");
    //   }
    // });
   }
});
