Template.shortDescription.helpers({
  shortDescription:function() {
    if(this.description.length > 300){
      shortDescription = this.description.substring(0,300);
      return shortDescription;
    }
  },
});

Template.shortDescription.events({
  'click .jsShortDescriptionToggle': function() {
    $('.box-description-' + this._id).toggle();
  },
});
