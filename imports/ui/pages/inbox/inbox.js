Template.inbox.helpers({

  incomingRequests: function() {
    var incomingRequests = Requests.find(
      {$and:[
        {ownerId: {$ne: Meteor.user()._id}},
        {receiverIds: Meteor.userId()},
        {status: "Open"}
        // DEV: not replied
      ]}
    ).fetch();

    return _.sortBy(incomingRequests,'timestamp').reverse();
  },
});

Template.inbox.onRendered(function() {

});

Template.inbox.events({

});
