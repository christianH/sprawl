
function submitProfileEditBox(form) {
  var userId = Meteor.user()._id;
  var username = Meteor.user().username;
  var firstname = form.firstname.value;
  var lastname = form.lastname.value;
  var homeaddress = form.homeaddress.value;
  var description = form.description.value;


  Meteor.call('getLatLong', homeaddress, function(err, location) {
    if (err) {
      alert(err.reason);
    } else {
      console.log("Got LatLong for Address");

      var userData = {
        userId: userId,
        username: username,
        firstname: firstname,
        lastname: lastname,
        homeaddress: homeaddress,
        homelocation: location,
        description: description,
      };

      Meteor.call('editAccount', userData, function(err, result) {
        if (err) {
          alert(err.reason);
        } else {
          Session.set('homeLocation', location);
          Session.set('homeAddress', homeaddress);
          Session.set('lastBox', null);
          Session.set('tempFileId', null);
          Session.set('activeBox', "profileBox");
          console.log("*** Updated Profile successfully ***");
        }
      });
    }
  });

}

Template.profileEditBox.onRendered(function() {
  this.$('.autosize').autosize();
  this.autorun(function () {
    if (GoogleMaps.loaded()) {
      $('.autocomplete').geocomplete();
    }
  });
});


Template.profileEditBox.helpers({
  userProfile: function() {
    return Meteor.users.findOne({_id: Session.get('profileUserId')});
  },
  // email: function() {
  //   return this.emails[0].address;
  // }
});

Template.profileEditBox.events({
  'click .jsEditProfilePicture': function(e) {
    e.preventDefault();
    $("#uploadProfilePicture").trigger('click');
  },
  'change .myFileInput': function(event, template) {
    var imageId = this.profile.details.picture;
    Meteor.call("deleteImage", imageId, function(error, result){
      if(error){
        console.log("error", error);
      }
    });
    FS.Utility.eachFile(event, function(file) {
      Images.insert(file, function (err, fileObj) {
        if (err){
           console.log(err);
        } else {
          var userId = Meteor.userId();
          var imageId = {
            "profile.details.picture": fileObj._id
          };
          Images.update(fileObj._id, {$set: {ownerId: userId}});
          Meteor.users.update(userId, {$set: imageId});
          console.log("*** Set Profile Picture successfully ***");
        }
      });
     });
   },
   'submit #profileEditBox': function(e) {
     e.preventDefault();
     submitProfileEditBox(e.target);
   },
});
