
Template.profileBox.helpers({
  profileOwner:function() {
    if(Session.get('profileUserId') === Meteor.userId()){
      return true;
    }
  },
});

Template.profileBox.events({
  'click .profileBox-stars': function(e) {
    e.preventDefault();
  },
  'click .jsScrolltoRatings': function() {
    var height = $('#topNav').outerHeight() + 24;
    $('html, body').animate({scrollTop: ($('#profileBox-ratings').offset().top - height)}, 'slow');
  },
  'click .jsProfileBoxIgnoreUser': function(e) {
    e.preventDefault();
    //DEV: Ignore User
  },

});
