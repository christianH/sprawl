
Template.userRatingOverall.onRendered(function() {
  this.$('.rateit').rateit();
});

Template.userRatingServices.onRendered(function() {
  this.$('.rateit').rateit();
});

Template.userRatingActivities.onRendered(function() {
  this.$('.rateit').rateit();
});
