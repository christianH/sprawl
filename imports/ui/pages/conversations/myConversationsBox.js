
Template.myConversationsBox.helpers({
  conversations: function() {
    return Notifications.find(
      {receiverId: Meteor.userId()},
      {sort: { timestamp: -1 }}
    );
  },
  unread: function() {
    if(this.status === "unread"){
      return true;
    }
  }
});

Template.myConversationsBox.events({
  'click .myConversationsBox-item-content': function(e) {
    e.preventDefault();
    var conversationData = this;
    Meteor.call('markMessagesAsRead', conversationData, function(error, result){
      if(error){
        console.log("error", error);
      }
    });
    Session.set('activeBox', "chatBox");
  },

});
