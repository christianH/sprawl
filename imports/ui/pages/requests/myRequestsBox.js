function deleteRequest(requestData){
  Session.set('delete', true);
  Meteor.call('deleteRequest', requestData, function(err, result) {
    if (err) {
      alert(err.reason);
    } else {
      console.log("*** Deleted Request successfully ***");
    }
  });
}

Template.myRequestsBox.helpers({
  myRequests: function() {
    return Requests.find(
      {ownerId: Meteor.user()._id},
      {sort: { timestamp: -1 }}
    );
  },
});

Template.myRequestsBox.onRendered(function() {

});

Template.myRequestsBox.events({
  'click .jsDeleteRequest': function (e) {
    e.preventDefault();
    Session.set('delete', true);
    deleteRequest(this);
  },
});
