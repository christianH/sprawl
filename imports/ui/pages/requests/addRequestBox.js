
function submitAddRequestBox(form) {

  var ownerId = Meteor.user()._id;
  var ownerName = Meteor.user().username;
  var status = "Open";
  var receiverIds = [];
  // DEV: validate hashtag
  var hashtag = form.searchHashtag_hashtag.value;
  var description = form.addDescription.value;
  // DEV: Description Placeholder
  if(!description) {
    description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ' +
    'sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, ' +
    'sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. ' +
    'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. ' +
    'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ' +
    'sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ' +
    'At vero eos et accusam et justo duo dolores et ea rebum. ' +
    'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
  }
  var address = Session.get("requestAddress");
  var location = Session.get("requestLocation");
  var tempFileId = Session.get('tempFileId');
  var datetype = $("input[name='date']:checked").val();
  var startdate;
  var starttime;
  var enddate;
  var endtime;
  var fixdate;
  var fixtime;
  var datetext;

  if(datetype === "asap") {
    startdate = moment().format('MM/DD/YYYY');
    starttime = moment().format('hh:mm a');
    datetext = "As soon as posible";
  }
  if(datetype === "duedate") {
    enddate = form.dueDate.value;
    endtime = moment(form.dueTime.value, 'HH:mm').format('hh:mm a');
    datetext = "Due: " + enddate + " at " + endtime;
  }
  if(datetype === "fixdate") {
    fixdate = form.fixDate.value;
    fixtime = moment(form.fixTime.value, 'HH:mm').format('hh:mm a');
    datetext = "Fix: " + fixdate + " at " + fixtime;
  }
  if(datetype === "range") {
    startdate = form.startDate.value;
    starttime = moment(form.startTime.value, 'HH:mm').format('hh:mm a');
    enddate = form.endDate.value;
    endtime = moment(form.endTime.value, 'HH:mm').format('hh:mm a');
    datetext = startdate + " at " + starttime + " - " + enddate + " at " + endtime;
  }

  var requestData = {
    ownerId: ownerId,
    ownerName: ownerName,
    hashtag: hashtag,
    description: description,
    loc: {
      type: "Point",
      coordinates: [location.long, location.lat]
    },
    address: address,
    date: {
      datetype: datetype,
      startdate: startdate,
      starttime: starttime,
      enddate: enddate,
      endtime: endtime,
      fixdate: fixdate,
      fixtime: fixtime,
      text: datetext
    },
    timestamp: Date.now() / 1000,
    status: status,
    receiverIds: receiverIds,
  };

  Meteor.call('createRequest', requestData, function(err, result) {
    if (err) {
      alert(err.reason);
    } else {
      console.log("Created Request");

      var requestId = result;
      requestData.requestId = requestId;

      if(tempFileId){
        var file = Tempfiles.findOne({_id:tempFileId});
        file.requestId = requestId;

        Attachments.insert(file, function (err, result) {
          if (err) {
            alert(err.reason);
          } else {
            console.log("Attachment saved in Collection");

            Tempfiles.remove({_id:tempFileId});
            console.log("Tempfile deleted");

            var attachmentId = result._id;
            var attachmentData = {
              attachmentId: attachmentId,
              requestId: requestId,
            };

            Meteor.call('addAttachment2NewRequest', attachmentData, function(err, result) {
              if (err) {
                alert(err.reason);
              } else {
                console.log("Added AttachmentId to Request");
              }
            });
          }
        });
      }

      Meteor.call('addReceiverIds2NewRequest', requestData, function(err, result) {
        if (err) {
          alert(err.reason);
        } else {
          console.log("*** Sent Request successfully ***");
        }
      });
      form.reset();
      Session.set('activeBox', "myRequestsBox");

    }
  });
}

Template.addRequestBox.onRendered(function() {

  //DEV: GeoLocation - Uncomment to use Geolocations (whenever AddRequestBox is rendered)
  // UI._globalHelpers.getGeoLocation();

  $('.autosize').autosize();
  this.autorun(function() {
    if (GoogleMaps.loaded()) {
      $('.autocomplete').geocomplete();
    }
  });

  // Datepicker
  $('#addRequest-dueDate').datepicker({
    prevText: "<i class='fa fa-chevron-left'></i>",
    nextText: "<i class='fa fa-chevron-right'></i>"
  });
  $('#addRequest-fixDate').datepicker({
    prevText: "<i class='fa fa-chevron-left'></i>",
    nextText: "<i class='fa fa-chevron-right'></i>"
  });
  $('#addRequest-startdate').datepicker({
    prevText: "<i class='fa fa-chevron-left'></i>",
    nextText: "<i class='fa fa-chevron-right'></i>",
    onSelect: function( selectedDate )
      {
        $('#addRequest-enddate').datepicker('option', 'minDate', selectedDate);
      }
  });
  $('#addRequest-enddate').datepicker({
    prevText: "<i class='fa fa-chevron-left'></i>",
    nextText: "<i class='fa fa-chevron-right'></i>",
    onSelect: function( selectedDate )
      {
        $('#addRequest-startdate').datepicker('option', 'maxDate', selectedDate);
      }
  });

});

Template.addRequestBox.helpers({
  tempAttachment: function() {
    return Tempfiles.findOne(Session.get('tempFileId'));
  }
});

Template.addRequestBox.events({
  'click .jsAddAttachment': function(e) {
    e.preventDefault();
    $("#uploadAttachment").trigger('click');
  },
  'change .myFileInput': function(event, template) {
      FS.Utility.eachFile(event, function(file) {
        Tempfiles.remove(Session.get('tempFileId'));
        Tempfiles.insert(file, function (err, fileObj) {
          if (err){
             console.log(err);
          } else {
            Tempfiles.update(
              {_id:fileObj._id},
              {$set: {ownerId: Meteor.userId()}}
            );
            Session.set('tempFileId',fileObj._id);
            console.log("Upload Temp successfull");
          }
        });
     });
   },
   'click .jsDeleteAttachment': function(e) {
     e.preventDefault();
     Tempfiles.remove(Session.get('tempFileId'));
   },
   'click .jsAddRequestBoxOptionsToggle': function(e) {
     e.preventDefault();
     $('#addRequestBoxOptions').show();
     $('.jsAddRequestBoxOptionsToggle').hide();
   },
   'click .jsAddRequestBoxReset':function(e) {
     e.preventDefault();
     $('#addRequestBox').trigger('reset');
     $('#addRequestBoxOptions').hide();
     $('.jsAddRequestBoxOptionsToggle').show();
   },
  'click .jsAddRequestBoxAsap': function() {
    $('.addRequestBox-dueDate').hide();
    $('.addRequestBox-fixDate').hide();
    $('.addRequestBox-range').hide();
  },
  'click .jsAddRequestBoxDueDate': function() {
    $('.addRequestBox-dueDate').show();
    $('.addRequestBox-fixDate').hide();
    $('.addRequestBox-range').hide();
  },
  'click .jsAddRequestBoxFixDate': function() {
    $('.addRequestBox-dueDate').hide();
    $('.addRequestBox-fixDate').show();
    $('.addRequestBox-range').hide();
  },
  'click .jsAddRequestBoxRange': function() {
    $('.addRequestBox-dueDate').hide();
    $('.addRequestBox-fixDate').hide();
    $('.addRequestBox-range').show();
  },
  'click .jsAddRequestBoxCurrentLocation': function() {
    $('.addRequestBox-otherLocation').hide();
  },
  'click .jsAddRequestBoxHomeLocation': function() {
    $('.addRequestBox-otherLocation').hide();
  },
  'click .jsAddRequestBoxOtherLocation': function() {
    $('.addRequestBox-otherLocation').show();
  },
  'submit #addRequestBox': function(e) {
    e.preventDefault();
    if(e.target.addRequestBox_currentLocation_toggle.checked){
      Session.set('requestLocation', Meteor.user().profile.details.geolocation);
      Session.set('requestAddress', Meteor.user().profile.details.geoaddress);
      submitAddRequestBox(e.target);
    }
    if(e.target.addRequestBox_homeLocation_toggle.checked){
      Session.set('requestLocation', Meteor.user().profile.details.homelocation);
      Session.set('requestAddress', Meteor.user().profile.details.homeaddress);
      submitAddRequestBox(e.target);
    }
    if(e.target.addRequestBox_otherLocation_toggle.checked){
      var otherAddress = $('#addRequestBox_otherLocation').val();
      var otherLocation = {lat: "", long: ""};
      geocoder = new google.maps.Geocoder();
      geocoder.geocode({'address': otherAddress}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          otherLocation.lat = results[0].geometry.location.lat();
          otherLocation.long = results[0].geometry.location.lng();
          Session.set('requestAddress', otherAddress);
          Session.set('requestLocation', otherLocation);
          submitAddRequestBox(e.target);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }
  },

});
