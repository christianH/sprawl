
Template.myNotificationsBox.helpers({
  notifications: function(){
    return Notifications.find(
      {receiverId: Meteor.userId()},
      {sort: { timestamp: -1 }}
    );
  },
  notificationContent: function(){
    if(this.type === "New Request") {
      if(this.status === "unread"){
        return Spacebars.SafeString('<b>New Request for ' + this.hashtag + '</b><br>by <a>@' + this.ownerName + '</a><span class="note"> &#183; </span><span class="note" data-livestamp="' + this.timestamp + '"></span>');
      } else {
        return Spacebars.SafeString('New Request for ' + this.hashtag + '<br>by <a>@' + this.ownerName + '</a><span class="note"> &#183; </span><span class="note" data-livestamp="' + this.timestamp + '"></span>');
      }
    }
  },
  statusUnread: function(){
    if(this.status === "unread"){
      return true;
    }
  },
});


Template.myNotificationsBox.events({
  'mouseenter .box-item': function(e) {
    $("#myNotificationsBox-item-xdiv-read-"+this._id).show();
    $("#myNotificationsBox-item-xdiv-delete-"+this._id).show();
  },
  'mouseleave .box-item': function(e) {
    $("#myNotificationsBox-item-xdiv-read-"+this._id).hide();
    $("#myNotificationsBox-item-xdiv-delete-"+this._id).hide();
  },
  'click .myNotificationsBox-item-xdiv-unread': function(e) {
    e.preventDefault();
    Session.set('changeNotification', true);
    Meteor.call('markNotificationAsRead', this._id, function(error, result){
      if(error){
        console.log("error", error);
      } else {
        Session.set('changeNotification', false);
      }
    });
  },
  'click .myNotificationsBox-item-xdiv-read': function(e) {
    e.preventDefault();
    Session.set('changeNotification', true);
    Meteor.call('markNotificationAsUnread', this._id, function(error, result){
      if(error){
        console.log("error", error);
      } else {
        Session.set('changeNotification', false);
      }
    });
  },
  'click .myNotificationsBox-item-xdiv-delete': function(e) {
    e.preventDefault();
    Session.set('changeNotification', true);
    Meteor.call('deleteNotification', this._id, function(error, result){
      if(error){
        console.log("error", error);
      } else {
        console.log("Notification deleted");
        Session.set('changeNotification', false);
      }
    });
  },
  'click .myNotificationsBox-item-content': function(e) {
    e.preventDefault();
    if(!Session.get('changeNotification')) {
      Meteor.call('markNotificationAsRead', this._id, function(error, result){
        if(error){
          console.log("error", error);
        }
      });
    }
  },

});
