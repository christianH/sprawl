function deleteSubscription(subscriptionData){
  Session.set('delete', true);
  Meteor.call('deleteSubscription', subscriptionData, function(err, result) {
    if (err) {
      alert(err.reason);
    } else {
      console.log("*** Unsubscribed to Hashtag successfully ***");
    }
  });
}

Template.mySubscriptionsBox.helpers({
  mysubscriptions: function() {
    return Subscriptions.find({}, {
      sort: { timestamp: -1 }
    });
  },
});

Template.mySubscriptionsBox.onRendered(function() {

});

Template.mySubscriptionsBox.events({
  'click .jsDeleteSubscription': function (e) {
    e.preventDefault();
    deleteSubscription(this);
  },

});
