
function submitAddSubscriptionBox(form) {
  var ownerId = Meteor.user()._id;
  var ownerName = Meteor.user().username;
  var hashtag = form.searchHashtag_hashtag.value;
  //DEV: validate Hashtag
  //DEV: Limit incoming requests
  //DEV: Set time Period
  var address = Meteor.user().profile.details.homeaddress;
  var location = Meteor.user().profile.details.homelocation;
  var radius = form.addSubscriptionBox_radius.value;
      radius = parseInt(radius, 10);

  var subscriptionData = {
    ownerId: ownerId,
    ownerName: ownerName,
    hashtag: hashtag,
    loc: {
      type: "Point",
      coordinates: [location.long, location.lat]
    },
    address: address,
    radius: radius,
    timestamp: Date.now() / 1000,
  };

  Meteor.call('createSubscription', subscriptionData, function(err, result) {
    if (err) {
      alert(err.reason);
    } else {
      Meteor.call('addReceiverId2MatchingRequests', subscriptionData, function(err, result) {
        if (err) {
          alert(err.reason);
        } else {
          console.log("*** Subscribed to Hashtag successfully ***");
        }
      });
      form.reset();
      Session.set('activeBox', "mySubscriptionsBox");
    }
  });
}

Template.addSubscriptionBox.onRendered(function() {

  //DEV: GeoLocation - Uncomment to use Geolocations (whenever addSubscriptionBox is rendered)
  // UI._globalHelpers.getGeoLocation();

  this.autorun(function () {
    $('#addSubscriptionBox-radius').ionRangeSlider({
      type: "single",
      min: 5,
      max: 500,
      step: 5,
      postfix: " km",
      max_postfix: "+",
      force_edges: true,
    });

  });

});


Template.addSubscriptionBox.events({
  'submit #addSubscriptionBox': function(e) {
    e.preventDefault();
    submitAddSubscriptionBox(e.target);
  }
});
