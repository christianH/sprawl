import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

import './body.html';

import './components/navigationBar/navigationBar.html';

import './pages/menu/menuBox.html';
import './pages/notifications/myNotificationsBox.html';

import './pages/login/loginBox.html';
import './pages/login/loginBox.js';
import './pages/login/signupBox.html';
import './pages/login/signupBox.js';
import './pages/login/passwordRecoveryBox.html';

import './pages/profile/profileBox.html';
import './pages/profile/profileEditBox.html';

import './pages/inbox/inbox.html';
import './pages/inbox/addReplyBox.html';

import './pages/conversations/chatBox.html';
import './pages/conversations/myConversationsBox.html';

import './pages/requests/addRequestBox.html';
import './pages/requests/myRequestsBox.html';

import './pages/subscriptions/addSubscriptionBox.html';
import './pages/subscriptions/mySubscriptionsBox.html';

import './pages/settings/mySettingsBox.html';

import './pages/policies/policiesBox.html';



Template.body.events({
  'click .meteor-status': function(e) {
    e.preventDefault();
    // DEV: show help for Offline Modus
    // Session.set('activeBox', "helpCenterBox");
  },
  'click .jsScrollTopSlow': function(e) {
    e.preventDefault();
    console.log("scroll");
    if(e.target.hash){
      var target = e.target.hash,
          $target = $(target);
      var navHeight = $('#topNav').outerHeight() + 30;
      $('html, body').stop().animate({scrollTop: $target.offset().top - navHeight},'slow');
    } else {
      $("html, body").animate({scrollTop: 0}, 'slow');
    }
  },
  'click .jsLogout': function(e) {
    e.preventDefault();
    Meteor.logout();
    FlowRouter.go('/login');
  },
  //DEV: Reset Collections
  'click .jsResetCollections': function(e) {
    e.preventDefault();
    var data = "";
    Meteor.call("jsResetCollections", data, function(error, result){
      if(error){
        console.log("error", error);
      } else {
         console.log("*** Reset Collections successfully ***");
      }
    });
  },

});
