var publicRoutes = FlowRouter.group({
  name: 'public',
});

var loggedInRoutes = FlowRouter.group({
  triggersEnter: [function(context, redirect) {
    //redirect to Login when not loggingIn
    var route = FlowRouter.current().route.group.name;
    if(route !== "public") {
      if(!Meteor.userId()) {
        console.log("Acces Denied! not loggedIn");
        FlowRouter.go('/login');
      }
    }
  }]
});

publicRoutes.route('/', {
  action: function() {
    BlazeLayout.render('body', {content: ''});
  }
});

publicRoutes.route('/login', {
  action: function() {
    BlazeLayout.render('body', {content: 'loginBox'});
  }
});

publicRoutes.route('/signup', {
  action: function() {
    BlazeLayout.render('body', {content: 'signupBox'});
  }
});

publicRoutes.route('/passwordRecovery', {
  action: function() {
    BlazeLayout.render('body', {content: 'passwordRecoveryBox'});
  }
});

publicRoutes.route('/policies', {
  action: function() {
    BlazeLayout.render('body', {content: 'policiesBox'});
  }
});

loggedInRoutes.route('/menu', {
  action: function() {
    BlazeLayout.render('body', {content: 'menuBox'});
  }
});

loggedInRoutes.route('/notifications', {
  action: function() {
    BlazeLayout.render('body', {content: 'myNotificationsBox'});
  }
});

loggedInRoutes.route('/profile', {
  action: function() {
    BlazeLayout.render('body', {content: 'profileBox'});
  }
});
loggedInRoutes.route('/profile/edit', {
  action: function() {
    BlazeLayout.render('body', {content: 'profileEditBox'});
  }
});

loggedInRoutes.route('/inbox', {
  action: function() {
    BlazeLayout.render('body', {content: 'inbox'});
  }
});

loggedInRoutes.route('/conversations', {
  action: function() {
    BlazeLayout.render('body', {content: 'myConversationsBox'});
  }
});

loggedInRoutes.route('/requests', {
  action: function() {
    BlazeLayout.render('body', {content: 'myRequestsBox'});
  }
});

loggedInRoutes.route('/subscriptions', {
  action: function() {
    BlazeLayout.render('body', {content: 'mySubscriptionsBox'});
  }
});

loggedInRoutes.route('/settings', {
  action: function() {
    BlazeLayout.render('body', {content: 'mySettingsBox'});
  }
});
