if ( Meteor.users.find().count() === 0 ) {
 Accounts.createUser({
   username: 'admin',
   email: 'c-hensen@gmx.de',
   password: 'admin',
   profile: {
     details: {
       firstname:'Christian',
       lastname: 'Hensen',
       homeaddress: 'Kerpenkath 24, 47626, Kevelaer, Germany',
       picture: false,
       description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
     },
     settings: {
       available: true,
       pushNotifications: true,
       emailNotifications: true,
       allowCurrentLocation: false,
     },
     ratings: {
       quality: 5,
       reliability: 5,
       overall: 5,
     }
    }
 });
console.log("Admin created with user:admin password:admin");

Accounts.createUser({
  username: 'test',
  email: 'test@test.de',
  password: 'test',
  profile: {
    details: {
      firstname:'Ivana',
      lastname: 'Rossi',
      homeaddress: 'Am Museum 2, 47623, Kevelaer, Germany',
      picture: false,
      description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    settings: {
      available: true,
      pushNotifications: true,
      emailNotifications: true,
      allowCurrentLocation: false,
    },
    ratings: {
      quality: 3,
      reliability: 4,
      overall: 3.5,
    }
   }
});
console.log("Test created with user:test password:test");

Accounts.createUser({
  username: 'test2',
  email: 'test2@test.de',
  password: 'test2',
  profile: {
    details: {
      firstname:'Jan',
      lastname: 'Müller',
      homeaddress: 'Hauptstraße 20, 47623, Kevelaer, Germany',
      picture: false,
      description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    settings: {
      available: true,
      pushNotifications: true,
      emailNotifications: true,
      allowCurrentLocation: false,
    },
    ratings: {
      quality: 5,
      reliability: 1,
      overall: 3,
    }
   }
});
console.log("Test2 created with user:test2 password:test2");
}

// if (Hashtags.find().count() === 0) {
//
//   Hashtags.insert({
//     _id: '#Activities',
//     name: 'Activities',
//     description: 'All Requests for Activities',
//     ancestors: []
//   });
//
//   Hashtags.insert({
//     _id: '#A(Sports)',
//     name: 'Sports',
//     description: 'All Requests for Sports',
//     ancestors: [
//       {_id:'#Activities', name:'Activities'}
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#A(Sports-Tennis)',
//     name: 'Tennis',
//     description: 'All Requests for Tennis',
//     ancestors: [
//       {_id:'#Activities', name:'Activities'},
//       {_id:'#A(Sports)', name:'Sports'}
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#A(Sports-Tennis-Single)',
//     name: 'Single',
//     description: 'All Requests for Tennis as a single player',
//     ancestors: [
//       {_id:'#Activities', name:'Activities'},
//       {_id:'#A(Sports)', name:'Sports'},
//       {_id:'#A(Sports-Tennis)', name:'Tennis'}
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#A(Leisure)',
//     name: 'Leisure',
//     description: 'All Requests for Leisure',
//     ancestors: [
//       {_id:'#Activities', name:'Activities'}
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#A(Leisure-Cinema)',
//     name: 'Cinema',
//     description: 'All Requests for Cinema',
//     ancestors: [
//       {_id:'#Activities', name:'Activities'},
//       {_id:'#A(Leisure)', name:'Leisure'},
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#Services',
//     name: 'Services',
//     description: 'All Requests for Services',
//     ancestors: []
//   });
//
//   Hashtags.insert({
//     _id: '#S(Housekeeping)',
//     name: 'Housekeeping',
//     description: 'All Requests for Housekeeping',
//     ancestors: [
//       {_id:'#Services', name:'Services'},
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#S(Housekeeping-Cleaning)',
//     name: 'Cleaning',
//     description: 'All Requests for Cleaning',
//     ancestors: [
//       {_id:'#Services', name:'Services'},
//       {_id:'#A(Housekeeping)', name:'Housekeeping'},
//     ]
//   });
//
//   Hashtags.insert({
//     _id: '#S(Webdevelopment)',
//     name: 'Webdevelopment',
//     description: 'All Requests for Webdevelopment',
//     ancestors: [
//       {_id:'#Services', name:'Services'},
//     ]
//   });
// }

process.env.MAIL_URL = 'smtp://postmaster@sandbox2dc3250c1c7e49ce88724397e6d03401.mailgun.org:8f2d33daf936bbd7ca2f23521018061b@smtp.mailgun.org:25';
